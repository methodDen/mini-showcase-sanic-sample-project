-- migrate:up

create table Booking(
    booking_id varchar(50) primary key,
    pnr varchar(30),
    expires_at date,
    phone char(12) not null,
    email varchar(50) not null,
    offer jsonb
);

create table Passengers(
    iin char(12) primary key,
    gender char(1) not null,
    type char(3) not null,
    first_name varchar(50) not null,
    last_name varchar(50) not null,
    date_of_birth date not null,
    citizenship char(2) not null,
    booking_id varchar(50) not null,
    foreign key (booking_id) references Booking(booking_id)
);

create table Document(
    number varchar(50) primary key,
    expires_at date not null,
    iin char(12) not null unique ,
    foreign key (iin) references Passengers(iin)
);


-- migrate:down


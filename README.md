# Mini-Showcase project

### Created by Daniyar Absatov


Email: daniyarabsatov@gmail.com

## Getting started

### To start project you should do the following steps:

- Clone this repository on your local machine
- Navigate into root folder
- Run "docker-compose up" or "docker-compose up -d" in your terminal (1st - starting project in terminal, 2nd - starting project as daemon)
- Send requests on given endpoints
- To kill container run "docker kill <container-id>"

## Endpoints

### Currently, the project supports following endpoints:

- localhost:8000/booking/ (POST)
- localhost:8000/booking/ (GET)
- localhost:8000/booking/account/ (GET)
- localhost:8000/booking/<booking_id> (GET)
- localhost:8000/offers/offer_id (GET)
- localhost:8000/search/ (POST)
- localhost:8000/search/search_id (GET)

### For POST requests with parameters you should use Postman 

### For GET requests you can run curl -X 'GET' http://{your_endpoint}

#### [Download Postman](https://www.postman.com/downloads/)
import aioredis
import asyncpg
import httpx
import ujson
import xmltodict
from sanic import Sanic
from code.utils import settings
from code import client
from code.blueprints.booking import booking_bp
from code.blueprints.offers import offer_bp
from code.blueprints.search import search_bp
from code.utils.settings import NATIONAL_BANK_URL
from datetime import date, time, timedelta
from sanic_scheduler import SanicScheduler, task
from sanic.exceptions import SanicException

app = Sanic('mini-showcase')
scheduler = SanicScheduler(app)
app.blueprint(offer_bp)
app.blueprint(booking_bp)
app.blueprint(search_bp)


async def load_rates(app):
    dt = date.today()
    async with client.HTTPClient() as cl:
        url = NATIONAL_BANK_URL.format(dt.strftime('%d.%m.%Y'))
        r: httpx.Response = await cl.get(url)

        if r.status_code != 200:
            raise SanicException('Cannot fetch rates from provider')

        parsed = xmltodict.parse(r.text)
        rates = {rate['title']: rate['description'] for rate in parsed['rates']['item']}

    await app.ctx.redis.set(f'currency_rates:{dt.isoformat()}', ujson.dumps(rates))


@app.listener("before_server_start")
async def init_before(app, loop):
    app.ctx.db_pool = await asyncpg.create_pool(dsn=settings.DATABASE_URL)
    app.ctx.redis = aioredis.from_url(settings.REDIS_URL, decode_responses=True,
                                      max_connections=50)


@app.listener('after_server_start')
async def load_rates_after_start(app, loop):
    await load_rates(app)


@app.listener("after_server_stop")
async def cleanup(app, loop):
    await app.ctx.redis.close()


# Default timezone: GMT
@task(timedelta(hours=24), time(hour=6, minute=0))
async def load_rates_scheduled(app):
    await load_rates(app)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)

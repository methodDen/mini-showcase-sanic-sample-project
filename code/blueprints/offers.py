import sanic.exceptions
import json
from sanic import Blueprint, response
from code.utils.settings import PROVIDERS

offer_bp = Blueprint("offer_blueprint", url_prefix='/offers', strict_slashes=True)


@offer_bp.get('/<search_id>/<offer_id>', strict_slashes=True)
async def get(request, search_id, offer_id):
    data_list = []
    for provider in PROVIDERS:
        provider_data = await request.app.ctx.redis.get(f'search:results:{provider}:{search_id}')
        if provider_data:
            data_list.append(provider_data)

    offer_li = []
    for provider_data in data_list:
        data = json.loads(provider_data)
        offers = {o['id']: o for o in data['items']}
        offer = offers.get(offer_id)
        offer_li.append(offer)

    if not offer_li[0]:
        raise sanic.exceptions.NotFound('No specified offer was found')

    return response.json(offer_li[0])

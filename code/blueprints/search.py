import uuid
import httpx
import json
import asyncio
import sanic.exceptions
import ujson

from code.utils.settings import REDIS_TTL, PROVIDERS, PROVIDER_TIMEOUT
from sanic import response, Blueprint
from code.validation.fields import search_data
from webargs_sanic.sanicparser import use_args
from datetime import date

search_bp = Blueprint("search_blueprint", url_prefix='/search', strict_slashes=True)


async def search_in_provider(redis, request_json, provider, search_id):
    redis_obj = {'status': 'PENDING'}
    await redis.setex(f'search:results:{provider}:{search_id}', REDIS_TTL, json.dumps(redis_obj))
    req = {
        "provider": provider,
        **request_json
    }

    items = []

    try:
        async with httpx.AsyncClient() as client:
            resp = await client.post('https://avia-api.k8s-test.aviata.team/offers/search',
                                     json=req, timeout=PROVIDER_TIMEOUT)
            resp = resp.json()
            items = resp['items']
    except httpx.TimeoutException:
        print(f'Timeout on provider {provider} was exceeded!')

    redis_obj = {'status': 'DONE',
                 'items': items}
    await redis.setex(f'search:results:{provider}:{search_id}', REDIS_TTL, json.dumps(redis_obj))


@search_bp.post('', strict_slashes=True)
@use_args(search_data)
async def post(request, args):
    search_id = str(uuid.uuid4())
    request_json = request.json
    for provider in PROVIDERS:
        asyncio.create_task(search_in_provider(request.app.ctx.redis,
                                               request_json,
                                               provider, search_id))
    return response.json({"id": search_id})


@search_bp.get('/<search_id>', strict_slashes=True)
async def get(request, search_id):
    currency = ''
    if request.args.get('currency'):
        currency = str(request.args.get('currency'))
        if not (len(currency) == 3) or not (currency.isupper()):
            raise sanic.exceptions.SanicException('Wrong currency format', status_code=422)

    dt = date.today()
    rates = await request.app.ctx.redis.get(f'currency_rates:{dt.isoformat()}')
    rates = ujson.loads(rates)

    providers_data = []
    search_status = 'DONE'
    for provider in PROVIDERS:
        provider_data = await request.app.ctx.redis.get(f'search:results:{provider}:{search_id}')
        provider_data = json.loads(provider_data)
        if len(currency.strip()) > 0:
            for d in provider_data['items']:
                if d['price']['currency'] == 'KZT':
                    if currency == 'KZT':
                        pass
                    else:
                        d['price']['amount'] = int(d['price']['amount'] / float(rates[currency]))
                        d['price']['currency'] = currency
                elif currency == 'KZT':
                    if d['price']['currency'] == 'KZT':
                        pass
                    else:
                        d['price']['amount'] = int(int(d['price']['amount']) *
                                                   float(rates[d['price']['currency']]))
                        d['price']['currency'] = currency
                else:
                    d['price']['amount'] = int(int(d['price']['amount']) * float(rates[currency])
                                               / float(rates[d['price']['currency']]))
                    d['price']['currency'] = currency

        if provider_data['status'] == 'PENDING':
            search_status = 'PENDING'
        else:
            if provider_data['items']:
                providers_data.append(provider_data['items'])
    if providers_data:
        return response.json({
            'status': search_status,
            'items': providers_data})

    raise sanic.exceptions.NotFound('No specified search_id was found')

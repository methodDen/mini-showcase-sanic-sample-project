import httpx
import json
from sanic import response, Blueprint
from datetime import datetime
from webargs_sanic.sanicparser import use_args
from code.validation.fields import booking_data
from code.utils.settings import format_data

booking_bp = Blueprint("booking_blueprint", url_prefix='/booking', strict_slashes=True)


@booking_bp.post('', strict_slashes=True)
@use_args(booking_data)
async def post(request, args):
    async with httpx.AsyncClient() as client:
        res = await client.post('https://avia-api.k8s-test.aviata.team/offers/booking',
                                    json=request.json)
        booking_res = res.json()

    booking_id = booking_res['id']
    pnr = booking_res['pnr']
    expires_at = booking_res['expires_at']
    expires_at = datetime.strptime(expires_at, '%Y-%m-%dT%H:%M:%S.%f%z')
    phone = booking_res['phone']
    email = booking_res['email']
    offer = booking_res['offer']
    passengers = booking_res['passengers']

    async with request.app.ctx.db_pool.acquire() as conn:
        async with conn.transaction():
            await conn.execute(
                'insert into Booking(booking_id, pnr, expires_at, phone, email, offer) '
                'values($1, $2, $3, $4, $5, $6)',
                booking_id, pnr, expires_at, phone, email, json.dumps(offer))
            for passenger in passengers:
                iin = passenger['document']['iin']
                gender = passenger['gender']
                passenger_type = passenger['type']
                first_name = passenger['first_name']
                last_name = passenger['last_name']
                date_of_birth = passenger['date_of_birth']
                date_of_birth = datetime.strptime(date_of_birth, '%Y-%m-%d')
                citizenship = passenger['citizenship']

                doc_number = passenger['document']['number']
                doc_expires_at = passenger['document']['expires_at']
                doc_expires_at = datetime.strptime(doc_expires_at, '%Y-%m-%d')

                await conn.execute(
                    'insert into Passengers(iin, gender, type, first_name, last_name, '
                    'date_of_birth, citizenship, booking_id) '
                    'values($1, $2, $3, $4, $5, $6, $7, $8)',
                    iin, gender, passenger_type, first_name,
                    last_name, date_of_birth, citizenship, booking_id)
                await conn.execute(
                    'insert into Document(iin, number, expires_at) values($1, $2, $3)',
                    iin, doc_number, doc_expires_at)

    return response.json(booking_res)


@booking_bp.get('', strict_slashes=True)
async def get_all(request):
    if request.args.get('limit') and request.args.get('offset'):
        async with request.app.ctx.db_pool.acquire() as conn:
            data_list = await conn.fetch(
                'select * from Booking b inner join Passengers p '
                'on p.booking_id = b.booking_id '
                'inner join Document d on d.iin = p.iin limit $1 offset $2',
                int(request.args.get('limit')), int(request.args.get('offset')))
        data_list = format_data(data_list)

    if request.args.get('email') and request.args.get('phone'):
        email = request.args.get("email")
        phone = request.args.get("phone")
        phone = '+' + phone.strip()
        async with request.app.ctx.db_pool.acquire() as conn:
            data_list = await conn.fetch(
                'select * from Booking b '
                'inner join Passengers p on p.booking_id = b.booking_id '
                'inner join Document d on d.iin = p.iin '
                'where b.email = $1 and b.phone = $2',
                email, phone)
            data_list = format_data(data_list)

    return response.json(data_list, dumps=json.dumps, default=str)


@booking_bp.get('/<booking_id>', strict_slashes=True)
async def get_by_id(request, booking_id):
    async with request.app.ctx.db_pool.acquire() as conn:
        data_list = await conn.fetch(
            'select * from Booking b '
            'inner join Passengers p on p.booking_id = b.booking_id '
            'inner join Document d on d.iin = p.iin '
            'where b.booking_id = $1',
            booking_id)
        data_list = format_data(data_list)
    return response.json(data_list, dumps=json.dumps, default=str)

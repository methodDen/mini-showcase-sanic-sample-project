from webargs import fields, validate
from code.validation.customfields import PhoneNumber, Email

booking_data = {
    "offer_id": fields.Str(required=True),
    "phone": PhoneNumber(required=True),
    "email": Email(required=True),
    "passengers":
        fields.Nested({
            "gender": fields.Str(required=True, validate=validate.OneOf(["M", "F"])),
            "type": fields.Str(required=True, validate=validate.OneOf(["ADT", "CHD", "INF"])),
            "first_name": fields.Str(required=True),
            "last_name": fields.Str(required=True),
            "date_of_birth": fields.Date(required=True),
            "citizenship": fields.Str(required=True, validate=validate.Length(equal=2)),
            "document": fields.Nested({
                "number": fields.Str(required=True),
                "expires_at": fields.Date(required=True),
                "iin": fields.Str(required=True, validate=validate.Length(equal=12))
            }, required=True)
        }, required=True, many=True)

}

search_data = {
    "cabin": fields.Str(required=True, validate=validate.OneOf(['Economy', 'Business'])),
    "origin": fields.Str(required=True, validate=validate.Length(equal=3)),
    "destination": fields.Str(required=True, validate=validate.Length(equal=3)),
    "dep_at": fields.Date(required=True),
    "arr_at": fields.Date(),
    "adults": fields.Int(required=True),
    "children": fields.Int(required=False, load_default=0),
    "infants": fields.Int(required=False, load_default=0),
    "currency": fields.Str(required=True, validate=validate.Length(equal=3))
}

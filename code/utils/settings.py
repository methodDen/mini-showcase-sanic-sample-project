import os
import json

DATABASE_URL = os.environ.get('DATABASE_URL')
REDIS_URL = os.environ.get('REDIS_URL')
REDIS_TTL = 1200
REDIS_OFFER_TTL = 900
PROVIDERS = ['Amadeus', 'Sabre']
PROVIDER_TIMEOUT = 30.0
NATIONAL_BANK_URL = 'https://www.nationalbank.kz/rss/get_rates.cfm?fdate={}'


def format_data(data_list):
    data_list = [{"id": d["booking_id"],
                  "pnr": d["pnr"],
                  "expires_at": d["expires_at"],
                  "phone": d["phone"],
                  "email": d["email"],
                  "offer": json.loads(d["offer"]),
                  "passenger": {
                      "gender": d["gender"],
                      "type": d["type"],
                      "first_name": d["first_name"],
                      "last_name": d["last_name"],
                      "date_of_birth": d["date_of_birth"],
                      "citizenship": d["citizenship"],
                      "document": {
                          "number": d["number"],
                          "expires_at": d["expires_at"],
                          "iin": d["iin"]
                      }
                  }} for d in data_list]

    return data_list




